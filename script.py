from ftplib import FTP
from simple_salesforce import Salesforce
import csv
import os
import re

#####

def main():

    ## Connect to Salesforce

    username = #REDACTED
    password = #REDACTED
    isSandbox = 0
    token = #REDACTED
    instance = #REDACTED

    sf = Salesforce(username=username, password=password, security_token=token, instance_url=instance,sandbox=isSandbox)

    ## Script attempts to run the query and will only continue if it was successful.

    start_date = input("Input Start Date in the format yyyy-mm-dd.\n")
    end_date = input("\nInput End Date in the format yyyy-mm-dd.\n")

    try:

        results = sf.query("SELECT "
            "LoadNumOnly__c, "
            "rtms__Expected_Ship_Date2__c, "
            "rtms__Expected_Delivery_Date2__c, "
            "First_Stop_City__c, "
            "First_Stop_State__c, "
            "First_Stop_Zip5__c, "
            "Last_Stop_City__c, "
            "Last_Stop_State__c, "
            "Last_Stop_Zip5__c, "
            "rtms__Carrier_Quote_Total__c, "
            "rtms__Equipment_Type__r.name, "
            "rtms__Mode_Name__c, "
            "Spot_Rate__c, "
            "rtms__Distance_Miles__c, "
            "rtms__Total_Weight__c, "
            "Hazmat_Required__c "
            "FROM rtms__Load__c "
            "WHERE "
            "rtms__Expected_Ship_Date2__c > " + start_date + " and rtms__Expected_Ship_Date2__c < " + end_date + "") 

    except:

        print("Error in your date format. Please try again.\n")
        main()

   ### Declare lists and counter ###

    value_list = []
    placeholder_list = []
    counter = 0

    ## Iterate over results and place them in to lists.

    for i, record in enumerate(results['records']):

        for key, value in record.items():

            if key == 'attributes':
                continue # Skips the metadeta

            ## Counter is the length of an Excel row. When the function resets the counter to 0, a new row is created

            if counter <= 16:
                placeholder_list.append(value)
                counter += 1

                if counter == 16:

                    ## Replace values

                    for x, i in enumerate(placeholder_list):

                        if i == 'Truckload' or i == 'Flatbed' or i == 'Refrigerated' or i == 'Drayage':
                            placeholder_list[x] = 'TL'

                        if "Dry Van 53'" in str(i):
                            placeholder_list[x] = "Dry Van 53'"
                            placeholder_list.append('V')
                        elif "Reefer 53'" in str(i):
                            placeholder_list[x] = "Reefer 53'"
                            placeholder_list.append('R')
                        elif "Flatbed 2 Axle 48'" in str(i):
                            placeholder_list[x] = "Flatbed 2' Axle 48'"
                            placeholder_list.append('F')
                        elif "Power Only" in str(i):
                            placeholder_list[x] = 'Power Only Van'
                            placeholder_list.append('P')
                        elif "Containers" in str(i):
                            placeholder_list[x] = "Containers 53'"
                            placeholder_list.append('C')

                    ## Insert placeholder list in to main list, and then clear the placeholder

                    value_list.append(placeholder_list)
                    placeholder_list = []
                    counter = 0

    header = ['Load Number','Expected Ship Date','Expected Delivery Date','First Stop City','First Stop State','First Stop Zip5',
        'Last Stop City','Last Stop State','Last Stop Zip5','Carrier Quote Total','Equipment Type',
        'Mode','Spot Rate','Distance(Miles)','Total Weight','HazMat', 'Equipment Code'
        ]

    ## Write headers and data to a CSV

    with open("output.csv", "w") as f: ## Output.csv is the name of the file that will be created. If a file already exists, it will be overwitten.
        writer = csv.writer(f)
        writer.writerow(header)
        for row in value_list:
            writer.writerow(row)

    ## Establish FTP Connection

    ftp = FTP(#REDACTED')
    ftp_user = #REDACTED
    ftp_pw = #REDACTED

    ftp.login(user=ftp_user, passwd=ftp_pw) # Login to server

    os.rename('output.csv', 'rates-327085-212793.csv') # Rename file
    filename = 'rates-327085-212793.csv'
    ftp.storbinary('STOR ' + filename, open(filename, 'rb'))
    ftp.quit()

    print("Operation Success. File Uploaded.")

#####

if __name__ == '__main__':
    main()